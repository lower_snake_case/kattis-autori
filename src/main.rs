fn main() {
    let mut buf: String = String::new();
    let mut res: String = String::new();

    std::io::stdin().read_line(&mut buf).unwrap();

    let slices = buf.trim().split('-');

    for slice in slices.into_iter() {
        res.push(slice.chars().nth(0).unwrap());
    }

    print!("{}\n", res);
}
